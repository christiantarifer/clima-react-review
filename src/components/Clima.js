import React from 'react';
import PropTypes from 'prop-types';

const Clima = ({ resultado }) => {

    // * EXTRACT VALUE
    const { name, main } = resultado;

    if (!name) return null;

    // Kelvin
    const kelvin = 273.15;

    let celsius = parseFloat(main.temp - kelvin, 10).toFixed(2);
    let celsiusMax = parseFloat(main.temp_max - kelvin, 10).toFixed(2);
    let celsiusMin = parseFloat(main.temp_min - kelvin, 10).toFixed(2);

    return (
        <div className="card-panel white col s12">
            <div className="black-text">
                <h2> El clima de {name} es: </h2>
                <p className="temperatura">
                    { celsius } <span> &#x2103; </span>
                </p>

                <p> Temperatura M&aacute;xima:
                    { celsiusMax } <span> &#x2103; </span>
                </p>

                <p> Temperatura M&iacute;nimo:
                    { celsiusMin } <span> &#x2103; </span>
                </p>
            </div>
        </div>
    );
}

Clima.propTypes = {
    resultado: PropTypes.object.isRequired
}
 
export default Clima;